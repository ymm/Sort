package cn.yuanmingming;

/**
 * Copyright (c) 2015 Acton(yuanmingming@foxmail.com)(yuanmingming@foxmail.com), All Rights Reserved.
 * 2015/6/18.
 */
public interface ISort {
    void sort();
}
