package cn.yuanmingming;

/**
 * Copyright (c) 2015 Acton(yuanmingming@foxmail.com), All Rights Reserved.
 * 2015/6/25.
 */
public class SelectSort extends BaseSort {
    public SelectSort() {
        super();
        System.out.println("选择排序：");
    }

    @Override
    public void sort() {
        for (int i = 0; i < data.length; i++) {
            int selected = i;
            for (int j = i + 1; j < data.length; j++) {
                if (data[j] < data[selected]) {
                    selected = j;
                }
            }
            if (selected != i) {
                int tmp = data[i];
                data[i] = data[selected];
                data[selected] = tmp;
            }
        }
    }
}
