package cn.yuanmingming;

/**
 * Copyright (c) 2015 Acton(yuanmingming@foxmail.com), All Rights Reserved.
 * 2015/6/18.
 */
public abstract class BaseSort implements ISort {
    public int[] data;

    public BaseSort() {
        data = new int[]{5, 58, 4, 69, 45, 2, 4, 78, 35, 19, 88};
//        System.out.println("原始数列:");
//        print();
    }

    public void print() {
        for (int key : data)
            System.out.print(key + " ");
        System.out.println();
    }
}
