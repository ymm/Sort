package cn.yuanmingming;

/**
 * Copyright (c) 2015 Acton(yuanmingming@foxmail.com), All Rights Reserved.
 * 2015/6/18.
 */
public class InsertSort extends BaseSort {
    public InsertSort() {
        super();
        System.out.println("插入排序：");
    }

    @Override
    public void sort() {
        int i, j;
        int tmpKey;
        for (i = 1; i < data.length; i++) {
            tmpKey = data[i];
            for (j = i - 1; j >= 0; j--) {
                if (data[j] > tmpKey) {
                    data[j + 1] = data[j];
                } else {
                    break;
                }
            }
//            j = i - 1;
//            while (j >= 0 && data[j] > tmpKey) {
//                data[j + 1] = data[j];
//                j--;
//            }
            data[j + 1] = tmpKey;
        }
    }
}
