package cn.yuanmingming;

/**
 * Copyright (c) 2015 Acton(yuanmingming@foxmail.com), All Rights Reserved.
 * 2015/6/18.
 */
public class BubbleSort extends BaseSort {
    public BubbleSort() {
        super();
        System.out.println("冒泡排序：");
    }

    @Override
    public void sort() {
        int tmpKey;
        for (int i = data.length; i > 0; i--) {
            for (int j = 0; j < i - 1; j++) {
                if (data[j] > data[j + 1]) {
                    tmpKey = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = tmpKey;
                }
            }
        }
    }
}
