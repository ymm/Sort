package cn.yuanmingming;

public class Main {

    public static void main(String[] args) {
        {
            BaseSort sort = new InsertSort();
            sort.sort();
            sort.print();
        }

        {
            BaseSort sort = new BubbleSort();
            sort.sort();
            sort.print();
        }

        {
            BaseSort sort = new SelectSort();
            sort.sort();
            sort.print();
        }
    }
}
